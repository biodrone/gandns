# README #

### Purpose ###

Automatically updating GANDI DNS via the LiveDNS API. Designed to be run as a cron job.

### Arguments ###

Required Args | Description 
:--- | ---
`-k, --key`| Gandi API Key
`-n, --name` | The name of the record to action
`-a, --add` | Add a record+
`-r, --remove` | Remove a record+
`-u, --update` | Update a record+
`-d, --domain` | Domain to action++
`-z, --zone` | Zone file to use++
`-po, --pushover` | Enable pushover support using user supplied API key
`-pb, --pushbullet` | Enable pushbullet support using user supplied API key
  _+, ++ - Mutually Exclusive_
  
 Optional Args | Description
 :--- | ---
 `-l, --logpath`|   Path to log file. Default is script working dir
 
 ### Exit Codes ###
 `1 - Failed to Retrieve Public IP Address`