#!/usr/bin/python

import sys
import logging
import argparse
from urllib.request import urlopen
import requests
import json
from pushbullet import Pushbullet

# arbitrary settings, change here if you want test API or diff log loc
API_ENDPOINT = 'https://dns.api.gandi.net/api/v5'
LOG_LEVEL = logging.DEBUG
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
pushover_key = ''
pushbullet_key = ''


def main(argv):
    global pushover_key, pushbullet_key

    parser = argparse.ArgumentParser(usage='Change DNS Records on Gandi Domains.')
    parser.add_argument('-k', '--key', help='API key to use when connecting.', required=True)
    parser.add_argument('-l', '--logpath', help='Logpath to use. Default is working dir.')
    parser.add_argument('-n', '--name', help='The name of the record to action', required=True)
    parser.add_argument('-po', '--pushover', help='Pushover User Key if Required')
    parser.add_argument('-pb', '--pushbullet', help='Pushbullet User Key if Required')

    actions = parser.add_mutually_exclusive_group(required=True)
    actions.add_argument('-a', '--add', help='Add a record.', action='store_true')
    actions.add_argument('-r', '--remove', help='Remove a record.', action='store_true')
    actions.add_argument('-u', '--update', help='Update a record.', action='store_true')

    supplicants = parser.add_mutually_exclusive_group(required=True)
    supplicants.add_argument('-d', '--domain', help='Domain to Use')
    supplicants.add_argument('-z', '--zone', help='Zone File to Use (by name or number)')

    args = parser.parse_args()

    if not args.logpath:
        logfile = 'gandns.log'
    else:
        logfile = args.logpath

    logging.basicConfig(filename=logfile, level=LOG_LEVEL, format=FORMAT)
    logging.info(" Starting GanDNS...")

    api_key = args.key
    domain = args.domain
    record_name = args.name

    if args.pushover:
        pushover_key = args.pushover
        logging.debug("Pushover API Enabled. Using API Key " + pushover_key)
    if args.pushbullet:
        pushbullet_key = args.pushbullet
        logging.debug("Pushbullet API Enabled. Using API Key " + pushbullet_key)

    logging.debug("Using API Key: " + api_key)
    logging.debug("Using API Endpoint: " + API_ENDPOINT)
    logging.debug("Using Domain: " + domain)
    logging.debug("Using Record: " + record_name)

    if domain:
        domain = args.domain
    else:
        zone = args.zone

    url = API_ENDPOINT + '/domains/' + domain + '/records/' + record_name + '/A'

    if args.add:
        add(api_key, record_name, url)
    if args.remove:
        remove(api_key, record_name, url)
    if args.update:
        update(api_key, record_name, url)

    logging.info(" Action Complete, GanDNS Exiting...")


def add(api_key, record, url):
    logging.debug('Adding New Record \'{}\''.format(record))

    curIP = get_ip()
    payload = {"rrset_ttl": 1800, "rrset_values": [curIP]}
    headers = {"Content-Type": "application/json", "X-Api-Key": api_key}
    u = requests.post(url, data=json.dumps(payload), headers=headers)
    json_object = json.loads(u._content)

    if u.status_code == 201:
        logging.info(" Status Code: {}, {}. Added Record {} With Value {}.".format(u.status_code,
                                                                                   json_object['message'],
                                                                                   record,
                                                                                   curIP))
    else:
        logging.error("Error - HTTP Status Code: {} When Trying to Add Record {}.".format(u.status_code,
                                                                                          record))

    if pushover_key:
        pushover_data = {"token": "axcw8yu6xs1qni7cy6g9vt8t1fkt8w",
                         "user": pushover_key,
                         "message": "Record \'{}\' Added with IP {}.".format(record, curIP)}

        po_note = requests.post('https://api.pushover.net/1/messages.json', data=pushover_data)
        if po_note.status_code == 200:
            logging.debug("Pushover Notification Queued Successfully!")
        else:
            logging.error("Pushover Notification Failed! Please Check Your Pushover User Key.")

    if pushbullet_key:
        pb = Pushbullet(pushbullet_key)
        pb_note = pb.push_note("GandDNS Note", "Record \'{}\' Added with IP {}.".format(record, curIP))
        logging.debug("Pushed to Pushbullet")


def remove(api_key, record, url):
    logging.debug('Deleting Record \'{}\''.format(record))

    curIP = get_ip()
    headers = {"Content-Type": "application/json", "X-Api-Key": api_key}
    u = requests.delete(url, headers=headers)

    if u.status_code != 404:
        logging.info(" Status Code: {}. Deleted Record {}.".format(u.status_code,
                                                                   record))
    else:
        logging.error("Error - HTTP Status Code: {} When Trying to Remove Record {}.".format(u.status_code,
                                                                                             record))

    if pushover_key:
        pushover_data = {"token": "axcw8yu6xs1qni7cy6g9vt8t1fkt8w",
                         "user": pushover_key,
                         "message": "Record \'{}\' with IP {} Deleted.".format(record, curIP)}

        po_note = requests.post('https://api.pushover.net/1/messages.json', data=pushover_data)
        if po_note.status_code == 200:
            logging.debug("Pushover Notification Queued Successfully!")
        else:
            logging.error("Pushover Notification Failed! Please Check Your Pushover User Key.")

    if pushbullet_key:
        pb = Pushbullet(pushbullet_key)
        pb_note = pb.push_note("GandDNS Note", "Record \'{}\' with IP {} Deleted.".format(record, curIP))
        logging.debug("Pushed to Pushbullet")


def update(api_key, record, url):
    global pushover_key, pushbullet_key

    logging.debug('Updating Record \'{}\''.format(record))

    headers = {"X-Api-Key": api_key}
    u = requests.get(url, headers=headers)
    json_object = json.loads(u._content)

    if u.status_code == 200:
        oldIP = str(json_object['rrset_values']).strip('\' [ ]')
        curIP = get_ip()

        logging.debug('Old IP: {}'.format(oldIP))
        logging.debug('New IP: {}'.format(curIP))

    else:
        logging.error("Current Record Query for \'{}\' Failed with Status Code {} {}.".format(record,
                                                                                              u.status_code,
                                                                                              json_object['message']))
        return

    if oldIP != curIP:
        # do the updaterings
        payload = {"rrset_ttl": 1800, "rrset_values": [curIP]}
        headers = {"Content-Type": "application/json", "X-Api-Key": api_key}
        u = requests.put(url, data=json.dumps(payload), headers=headers)
        json_object = json.loads(u._content)

        if u.status_code == 201:
            logging.info(' Status Code: {}, {}. IP Updated For {} From {} to {}.'.format(u.status_code,
                                                                                        json_object['message'],
                                                                                        record,
                                                                                        oldIP,
                                                                                        curIP))
        else:
            logging.error('Error: HTTP Status Code {}, When Updating {}.'.format(u.status_code,
                                                                                 record))

        if pushover_key:
            pushover_data = {"token": "axcw8yu6xs1qni7cy6g9vt8t1fkt8w",
                             "user": pushover_key,
                             "message": "Record {} Updated from {} to {}".format(record, oldIP, curIP)}

            po_note = requests.post('https://api.pushover.net/1/messages.json', data=pushover_data)
            if po_note.status_code == 200:
                logging.debug("Pushover Notification Queued Successfully!")
            else:
                logging.error("Pushover Notification Failed! Please Check Your Pushover User Key.")

        if pushbullet_key:
            pb = Pushbullet(pushbullet_key)
            pb_note = pb.push_note("GandDNS Note", "Record {} Updated From {} To {}.".format(record, oldIP, curIP))
            logging.debug("Pushed to Pushbullet")

    else:
        logging.info(" IP Address Matches, Not Updating...")


def get_ip():
    publicIP = urlopen("https://icanhazip.com").read()
    if publicIP != "":
        return str(publicIP, 'utf-8').strip()
    else:
        logging.error("Public IP Not Found. HALP!")
        sys.exit(1)

main(sys.argv)
